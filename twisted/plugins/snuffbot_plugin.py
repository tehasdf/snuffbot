from zope.interface import implements

from twisted.python import usage
from twisted.plugin import IPlugin
from twisted.application.service import IServiceMaker
from twisted.application import internet

from snuffbot import SnuffBotFactory


class Options(usage.Options):
    optParameters = [
        ['HOST', 'h', 'irc.rizon.net', 'IRC server to connect'],
        ['PORT', 'p', 6667, 'Port to connect to the irc server on', int],
        ['channel', 'c', '#snuffbottest', 'Channel'],
        ['nickname', 'n', 'Snuffbot', 'Nick'],
        ['password', 'w', 'foo', 'Server password'],
        ['dbname', 'd', 'database.sqlite3', 'Database to use'],
    ]


class SnuffBotServiceMaker(object):
    implements(IServiceMaker, IPlugin)
    tapname = "snuffbot"
    description = "Snuffbot"
    options = Options

    def makeService(self, options):
        return internet.TCPClient(options['HOST'], options['PORT'], 
            SnuffBotFactory(**options))


serviceMaker = SnuffBotServiceMaker()