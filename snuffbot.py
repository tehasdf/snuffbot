#coding: utf-8
from collections import defaultdict
from itertools import izip_longest
import random
import re

from twisted.internet.defer import inlineCallbacks, Deferred
from twisted.internet import reactor, task, protocol
from twisted.words.protocols import irc
from twisted.enterprise import adbapi
from twisted.python import log

from base import BaseBotProtocol, requires, BaseBotFactory

class SnuffBotProtocol(BaseBotProtocol):
    callLater = reactor.callLater
    nickname = ''
    DONT_COUNT_SNUFFS = {'solo', 'solo2', 'solo3'}

    def signedOn(self):
        super(SnuffBotProtocol, self).signedOn()
        self.odliczanie = {}
        self.przerwali = defaultdict(set)
        self.nielicz = defaultdict(bool)

    def privmsg(self, user, channel, message):
        nickname, sep, rest = user.partition('!')

        if self.odliczanie.get(channel):
            self.przerwali[channel].add(nickname)
            return
        message = message.replace('\xe2\x86\x92', '!go ')
        return super(SnuffBotProtocol, self).privmsg(user, channel, message)

    # @requires('ohqv')
    @inlineCallbacks
    def command_GO(self, channel, nick, args):
        if not args:
            args = ['random']
        if self.names[channel][nick]:
            self.odliczanie[channel] = True
            try:
                yield self.odlicz(channel, nick, *args)
            except LookupError as e:
                self.say(channel, u"Nie ma odliczania %s!" % (args[0], ))
            else:
                if not self.nielicz[channel] and args[0].lower() not in self.DONT_COUNT_SNUFFS:
                    self.addToTopic(channel)

                if self.przerwali[channel]:
                    self.say(channel, "%s, nie przerywaj odliczania!" % (
                        ', '.join(self.przerwali[channel]), )
                    )
            finally:
                self.odliczanie[channel] = False
                self.przerwali[channel] = set()

    command_PAL = command_GO

    @requires('ohqv')
    def command_DOWN(self, channel, nick, args):
        if self.names[channel][nick]:
            self.addToTopic(channel, -1)

    @requires('ohqv')
    def command_UP(self, channel, nick, args):
        if self.names[channel][nick]:
            self.addToTopic(channel)

    @requires('ohqv')
    def command_LICZ(self, channel, nick, args):
        if args and self.names[channel][nick]:
            if args[0] in {'+', 'y', 'tak', 'on'}:
                self.nielicz[channel] = False
                self.say(channel, 'Będę znowu liczył snuffy na %s' % channel)
            if args[0] in {'-', 'n', 'nie', 'off'}:
                self.nielicz[channel] = True
                self.say(channel, 'Nie będę teraz liczył snuffów na %s' % channel)

    @inlineCallbacks
    def command_HELP(self, channel, nick, args):
        odliczania = yield self.factory.dbpool.runQuery(
                'select distinct(name) from odliczania')
        odliczania = [result[0] for result in odliczania]

        indices = range(0, len(odliczania), 30)
        indices = izip_longest(indices, indices[1:])
        odliczania = iter([odliczania[s:e] for s, e in indices])

        message = [
            u"Dostępne odliczania to: %s" % (', '.join(next(odliczania)), )
        ]

        for odl_sublist in odliczania:
            message.append(u"... %s" % (', '.join(odl_sublist), ),)

        message.append(u"Pozostałe komendy to !licz (+/-), !up, !down")

        for phrase in message:
            self.notice(nick, phrase.encode('utf-8'))
            yield self.wait(0.5)

    @inlineCallbacks
    def odlicz(self, channel, nick, odliczanie, *args):

        if odliczanie == u'random':
            odliczania = yield self.factory.dbpool.runQuery(
                'select distinct(name) from odliczania')


            if args:
                random.seed(''.join(args))
            while odliczanie == 'random' or odliczanie in self.DONT_COUNT_SNUFFS:
                odliczanie, = random.choice(odliczania)

        lines = yield self.factory.dbpool.runQuery(
            'select line, wait from odliczania where name=? order by linenum desc',
            (odliczanie.lower(), )
        )
        if not lines:
            raise LookupError(odliczanie)
        for line, wait in lines:
            line = line.encode('utf-8') % {'nick': nick, 'channel': channel}
            self.say(channel, line)
            yield self.wait(wait)

    def addToTopic(self, channel, howMuch=1):
        topic = self.topics.get(channel)
        if topic is None:
            return
        num = next((word for word in topic.split() if word.isdigit()), None)
        if num is None:
            return
        added = str(int(num) + howMuch)
        topic = topic.replace(num, added, 1)
        if any(mode in self.names[channel][self.nickname] for mode in 'ohq'):
            self.topic(channel, topic)
        else:
            self.say(channel, '/topic %s' % (topic, ))

class SnuffBotFactory(BaseBotFactory):
    protocol = SnuffBotProtocol
