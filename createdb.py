import os
import re
import codecs

def toIrc(string):
    return (string
        .replace('%B%', '\x02')
        .replace('%U%', '\x1F')
        .replace('%nick', '%(nick)s')
        .rstrip('\n')
    )

def fromdir(directory):
    snuffs = {}
    for filename in os.listdir(directory):
        name, ext = os.path.splitext(filename)
        try:
            with codecs.open(os.path.join(directory, filename), 'r', 'latin-1') as f:
                lines = [toIrc(line) for line in f]
            snuffs[name] = (1, lines)
        except IOError:
            snuffs.pop(name, None)
    return snuffs

def fromtcl(filename):
    snuffs = {}
    with open(filename) as f:
        for line in f:
            r = re.search('proc pub_([a-zA-Z0-9]+) ', line)
            if r is not None:
                name = r.group(1)
                current = name
                snuffs[name] = (1, [])

            r = re.search('\$chan : (.*)"', line)
            if r is not None and not line.strip().startswith('#'):
                try:
                    snuffs[current][1].append(r.group(1).decode('string_escape').decode('latin-1'))
                except UnicodeDecodeError:
                    print line
    return snuffs

def delete_db(conn):
    conn.execute('drop table if exists odliczania')

def create_db(conn):
    conn.execute('create table odliczania (name text, line text, linenum integer, wait real, primary key (name, linenum))')
    conn.commit()

def insert_snuffs(conn, snuffs):
    conn.executemany('insert into odliczania values (?, ?, ?, ?)', snuffs)
    conn.commit()

def transform(name, line, count, wait):
    """Pogrub linijke mowiaca 'Snuff!' (tj ostatnia linijke)"""

    if count == 1:
        line = line.replace('\x02', '')
        line = u'\x02%s\x02' % (line,)
    return (name, line, count, wait)

def todb(snuffs):
    snuffs = [transform(name, line, len(lines) - num, wait) for name, (wait, lines) in snuffs.iteritems() for num, line in enumerate(lines)]    
    return snuffs

def update(old, new):
    for key in new:
        original_key = key
        if key in old:
            count = 2
            while "%s%d" % (key, count) in old:
                count += 1

            old[key + unicode(count)] = new[original_key]
        else:
            old[key.decode('utf-8')] = new[key]

if __name__ == '__main__':
    import sqlite3
    conn = sqlite3.connect('database.sqlite3')
    delete_db(conn)
    create_db(conn)
    snuffs = fromdir('odliczania')
    other = fromtcl('snuff.tcl')
    third = fromdir('snuffs')
    update(snuffs, third)
    update(snuffs, other)
    snuffs = todb(snuffs)
    insert_snuffs(conn, snuffs)